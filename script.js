$(function() {
	function scrollToElement(selector, time, verticalOffset) {
	    time = typeof(time) != 'undefined' ? time : 'slow';
	    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	    element = $(selector);
	    offset = element.offset();
	    offsetTop = offset.top + verticalOffset;
	    $('html, body').animate({
	        scrollTop: offsetTop
	    }, time);
	}

	$("#buttonGenerate").click(function() {
		location.reload();
	});

	$('#buttonVecx').click(function () {
    	scrollToElement('#vecx', 500, -3);
	});

	$('#buttonPopBin').click(function () {
    	scrollToElement('#populationBin', 500, -3);
	});

	$('#buttonPopDec').click(function () {
    	scrollToElement('#populationDec', 500, -3);
	});

	$('#buttonTop').click(function () {
    	scrollToElement('#main', 500, -3);
	});
});