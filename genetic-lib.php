<?php

class genetic {
	private $P;
	private $N;
	private $B;
	public $min;
	public $max;
	public $indexMin;
	public $indexMax;
	public $populationBin = array(array(array()));
	public $populationDec = array(array());
	public $vecX = array();

	public function __construct($PP, $NN, $BB) {
		$this->P = $PP;
		$this->N = $NN;
		$this->B = $BB;
	}

	public function GeneratePopulation() {
		$this->vecX = array_fill(0, $this->P, '0');

		for($i = 0; $i < $this->P; $i++) {
            for($j = 0; $j < $this->N; $j++) {
                for($k = 0; $k < $this->B; $k++) {
                        $this->populationBin[$i][$j][$k] = rand(0,1);
                }
            }
        }
	}

	public function PopulationToDec() {
		for($i = 0; $i < $this->P; $i++) {
            for($j = 0; $j < $this->N; $j++) {      
                $this->populationDec[$i][$j] = bindec(implode("",  $this->populationBin[$i][$j])); 
            }
        }
	}

	public function ShowPopulation($type) {
		for($i = 0; $i < $this->P; $i++) {
			echo("<tr>");
                for($j = 0; $j < $this->N; $j++) {
                	echo("<td>");
                	if($type == 0) { 
                       	echo($this->populationDec[$i][$j]);
                    }
                    for($k = 0; $k < $this->B; $k++) {
                    	if($type == 1) {
                            echo($this->populationBin[$i][$j][$k]);
                    	} 
                    }
					echo("</td>");
                }
            echo("</tr>");
        }
	}
	public function MakeVecX() {
		for($i = 0; $i < $this->P; $i++) {
			for($j = 0; $j < $this->N; $j++) {
				$this->vecX[$i] += $this->populationDec[$i][$j] * $this->populationDec[$i][$j];
			}
		}
	}
	public function ShowVecX() {
		for($i = 0; $i < $this->P; $i++) {
			echo("<tr><td>" . $this->vecX[$i] . "</td></tr>");
		}
	}
	public function GetMinMax() {
		$this->min = min($this->vecX);
		$this->max = max($this->vecX);
		$this->indexMin = array_search($this->min, $this->vecX);
		$this->indexMin += 1;
		$this->indexMax = array_search($this->max, $this->vecX);
		$this->indexMax += 1;
	}
}
?>