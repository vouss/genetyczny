<!DOCTYPE html>
<html>
	<head>
		<title>Algorytm Genetyczny</title>
		<meta name="author" content="Marcin Boś">
		<link rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script type="text/javascript" src="script.js"></script>
	</head>
<body>
	<div id="header">
		<div id="buttonVecx" class="button">vector X</div>
		<div id="buttonPopDec" class="button">Population (dec)</div>
		<div id="buttonPopBin" class="button">Population (bin)</div>
		<div id="buttonTop" class="button">Scroll to top</div>
		<div id="buttonGenerate" class="button">GENERATE IT!</div>
	</div>
<?php 
	require('genetic-lib.php');
	$gen = new genetic(50,10,3);

	$gen->GeneratePopulation();
	$gen->PopulationToDec();
?>
	<div id="main">
		<div id="populationBin" class="container">
			<h3>Population (bin)</h3>
			<table>
				<?php $gen->ShowPopulation(1); ?>
			</table>
		</div>
		<div id="populationDec" class="container">
			<h3>Population (dec)</h3>
			<table>
				<?php $gen->ShowPopulation(0);?>
			</table>
		</div>
		<div id="vecx" class="container">
			<h3>Vector X</h3>
			<table>
				<?php 
					$gen->MakeVecX();
					$gen->ShowVecX();
					$gen->GetMinMax();
				?>
			</table>
		</div>
		<div class="container">
			MIN: <?php echo($gen->min . " index: " . $gen->indexMin ); ?>
			<br />
			MAX: <?php echo($gen->max . " index: " . $gen->indexMax ); ?>
		</div>
	</div>
</body>
</html>
